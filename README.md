# libre-hosters

A list of people and organizations who deploy, maintain and offer open source services to the public.

- [nekoea.red](https://nekoea.red)  
- [nomagic.uk](https://nomagic.uk)  
- [pixie.town](https://pixie.town)  
- [linux.pizza](https://linux.pizza)  
- [weho.st](https://weho.st)  
- [webionite.com](https://webionite.com)  
- [nixnet.services](https://nixnet.services)  
- [snopyta.org](https://snopyta.org)  
- [wtf.roflcopter.fr](https://wtf.roflcopter.fr)  
- [chaton.sequanux.org](https://chaton.sequanux.org)  
- [www.devloprog.org](https://www.devloprog.org)  
- [www.chapril.org](https://www.chapril.org)  
- [aktivix.org](https://aktivix.org)  
- [www.colibris-outilslibres.org](https://www.colibris-outilslibres.org)  
- [libreops.cc](https://libreops.cc)  
- [amipo.fr](https://amipo.fr)  
- [42l.fr](https://42l.fr)  
- [3hg.fr](https://3hg.fr)  
- [www.alolise.org](https://www.alolise.org)  
- [bastet.parinux.org](https://bastet.parinux.org)  
- [www.ilinux.fr](https://www.ilinux.fr)  
- [retzien.fr](https://retzien.fr)  
- [hadoly.fr](https://hadoly.fr)  
- [www.infini.fr](https://www.infini.fr)  
- [adminforge.de](https://adminforge.de)  
- [www.zici.fr](https://www.zici.fr)  
- [opendoor.fr](https://opendoor.fr)  
- [kabi.tk](https://kabi.tk)  
- [dismail.de](https://dismail.de)  
- [hostux.network](https://hostux.network)  
- [inex.rocks](https://inex.rocks)  
- [darkn.space](https://darkn.space)  
- [coletivos.org](https://coletivos.org)  
- [arn-fai.net](https://arn-fai.net)  
- [allmende.io](https://allmende.io)  
- [gozmail.bzh](https://gozmail.bzh)  
- [thing.net](https://thing.net)  
- [teknik.io](https://teknik.io)  
- [www.systemli.org](https://www.systemli.org)  
- [tchncs.de](https://tchncs.de)  
- [www.ggc-project.de](https://www.ggc-project.de)  
- [macaw.me](https://macaw.me)  
- [systemausfall.org](https://systemausfall.org)  
- [wiuwiu.de](https://wiuwiu.de)  
- [www.immerda.ch](https://www.immerda.ch)  
- [www.datenkollektiv.net](https://www.datenkollektiv.net)  
- [www.shelter.is](https://www.shelter.is)  
- [anonbox.net](https://anonbox.net)  
- [www.bugsi.de](https://www.bugsi.de)  
- [riseup.net](https://riseup.net)  
- [www.ffdn.org](https://www.ffdn.org)  
- [cozy.io](https://cozy.io)  
- [www.automario.eu](https://www.automario.eu)  
- [felinn.org](https://felinn.org)  
- [nota.live](https://nota.live)  
- [www.aquilenet.fr](https://www.aquilenet.fr)  
- [opportunis.me](https://opportunis.me)  
- [pwoss.org](https://pwoss.org)  
- [degrowth.net](https://degrowth.net)  
- [www.pofilo.fr](https://www.pofilo.fr)  
- [komun.org](https://komun.org)  
- [www.schnuud.de](https://www.schnuud.de)  
- [dssr.ch](https://dssr.ch)  
- [social.my-wan.de](https://social.my-wan.de)  
- [lavatech.top](https://lavatech.top)  
- [pussthecat.org](https://pussthecat.org)  
- [envs.net](https://envs.net)  
- [www.zapashcanon.fr](https://www.zapashcanon.fr)  
- [iswleuven.be](https://iswleuven.be)  
- [tilde.team](https://tilde.team)  
- [anjara.eu](https://anjara.eu)  
- [www.nikisoft.one](https://www.nikisoft.one)  
- [sp-codes.de](https://sp-codes.de)  
- [midov.pl](https://midov.pl)  
- [www.skyn3t.in](https://www.skyn3t.in)  
- [phreedom.club](https://phreedom.club)  
- [shittyurl.org](https://shittyurl.org)  
- [antopie.org](https://antopie.org)  
- [rita.moe](https://rita.moe)  
- [cats-home.net](https://cats-home.net)  
- [www.anchel.nl](https://www.anchel.nl)  
- [devol.it](https://devol.it)  
- [darmarit.org](https://darmarit.org)  
- [mdosch.de](https://mdosch.de)  
- [fossencdi.org](https://fossencdi.org)  
- [mint.lgbt](https://mint.lgbt)  
- [spot.ecloud.global](https://spot.ecloud.global)  
- [creep.im](https://creep.im)  
- [bbaovanc.com](https://bbaovanc.com)  
- [picasoft.net](https://picasoft.net)  
- [simple-web.org](https://simple-web.org)  
- [services.mstdn.social](https://services.mstdn.social)  
- [puffyan.us](https://puffyan.us)  
- [nfld.uk](https://nfld.uk)  
- [rollenspiel.monster](https://rollenspiel.monster)  
- [securetown.top](https://securetown.top)  
- [priv.pw](https://priv.pw)  
- [hyteck.de](https://hyteck.de)  
- [redeyes.site](https://redeyes.site)  
- [tildeverse.org](https://tildeverse.org)  
- [uncloud.do](https://uncloud.do)  
- [beparanoid.de](https://beparanoid.de)  
- [vern.cc](https://vern.cc)  
- [slipfox.xyz](https://slipfox.xyz)  
- [esmailelbob.xyz](https://en.esmailelbob.xyz)  
- [heimdall.pm](https://heimdall.pm)  
- [webair.xyz](https://webair.xyz)   
- [ilyamikcoder.com](https://ilyamikcoder.com)   
- [sev.monster](https://sev.monster)  
- [cthd.icu](https://cthd.icu)  
- [smnz.de](https://smnz.de)  
- [ononoki.org](https://ononoki.org)  
- [frlt.one](https://frlt.one)  
- [dcs0.hu](https://dcs0.hu)  
- [neet.works](https://neet.works)  
- [manerakai.com](https://manerakai.com)  
- [totaldarkness.net](https://totaldarkness.net)  
- [whatever.social](https://whatever.social)  
- [censors.us](https://censors.us)  
- [namazso.eu](https://namazso.eu)  
- [1d4.us](https://1d4.us)  
- [lacontrevoie.fr](https://lacontrevoie.fr)  
- [catalyst.sx](https://catalyst.sx)  
- [bus-hit.me](https://bus-hit.me)  
- [tiekoetter.com](https://tiekoetter.com)  
- [projectsegfau.lt](https://projectsegfau.lt)  
- [kavin.rocks](https://kavin.rocks)  
- [weblibre.org](https://weblibre.org)  
- [privacydev.net](https://privacydev.net)  
- [madreyk.xyz](https://madreyk.xyz)  
- [dhusch.de](https://dhusch.de)  
- [riverside.rocks](https://riverside.rocks)  
- [anonymousland.org](https://anonymousland.org)  
- [jae.fi](https://jae.fi)  
- [evv1l.space](https://evv1l.space/ru/services)  
- [leemoon.network](https://leemoon.network)  
- [sadium.cyou](https://sadium.cyou)  
- [foxhaven.cyou](https://foxhaven.cyou)  
- [thedroth.rocks](https://thedroth.rocks)  
- [the-sauna.icu](https://the-sauna.icu)  
- [bloat.cat](https://bloat.cat)  
- [drgnz.club](https://drgnz.club)  
- [comfycamp.space](https://comfycamp.space)  
- [ej.cabal.run](https://ej.cabal.run)  
- [lunar.icu](https://lunar.icu)  
- [unix.dog](https://unix.dog)  
- [dimension](https://dimension.sh)  
- [mezzo](https://mezzo.moe)  
- [r4fo](https://r4fo.com)  
- [nadeko](https://nadeko.net)  
- [getimiskon](https://getimiskon.xyz)  
- [sysrq](https://sysrq.in)  

## License  
[WTFPL](LICENSE)  

